# frozen_string_literal: true

require 'json'

class FakeGateway
  DOC_PATH = File.expand_path(File.join(__dir__, '..', '..', 'api', 'docs', 'api.md'))

  def initialize
    @api_docs = File.readlines(DOC_PATH)
  end

  def describe_endpoint(_)
    doc_response('api-api-endpoints-show')
  end

  def get_endpoint(_)
    doc_response('api-api-endpoints-show')
  end

  def delete_endpoint(_)
    doc_response('api-api-endpoints-delete').body
  end

  def get_health_summary
    doc_response('api-api-health-index')
  end

  def get_incidents
    doc_response('api-api-incidents-index')
  end

  def post_manifest(manifest)
    status = manifest.dig('spec', 'name').include?('create') ? 201 : 200
    doc_response('api-api-manifest-apply', status)
  end

  private

  def doc_response(link_id, status_code = 200)
    response         = []
    found_block      = false
    in_json_response = false
    in_request_block = false
    completed        = false

    @api_docs.each do |line|
      found_block = true if line.include? "id=#{link_id}"

      in_request_block = found_block && true if line.include? 'Response'

      if found_block && in_request_block && line.include?('```json')
        in_json_response = true
        next
      end

      if in_json_response && !completed
        if line.chomp == '```'
          completed        = true
          in_json_response = false
          in_request_block = false
        else
          response << line.strip
        end
      end
    end

    OpenStruct.new(
      body:
        response
          .join
          .delete("\n"),
      status: OpenStruct.new(code: status_code)
    )
  end
end
