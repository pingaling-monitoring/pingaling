require 'yaml'

class ManifestFactory
  SAMPLE_YAML_DIR = File.expand_path(File.join(__dir__, '..', '..', 'doc'))

  class << self
    def create_single_resource
      manifest = YAML.load_file(File.join(SAMPLE_YAML_DIR, 'cronjob_sample.yml'))
      name = manifest.dig('spec', 'name') + '-create'

      manifest
        .merge({'spec' => { 'name' => name}})
        .to_yaml
    end

    def update_single_resource
      File.read(File.join(SAMPLE_YAML_DIR, 'endpoint_sample.yml'))
    end

    def update_slack_channel
      File.read(File.join(SAMPLE_YAML_DIR, 'slack_sample.yml'))
    end

    def stream_of_manifests
      [
        create_single_resource,
        update_single_resource,
        update_slack_channel,
      ].join("\n")
    end
  end
end
