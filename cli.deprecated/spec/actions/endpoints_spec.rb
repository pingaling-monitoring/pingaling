require 'spec_helper'
require 'actions/endpoints'

RSpec.describe Actions::Endpoints do
  subject { described_class.new }

  let(:expected) { "pending    https://service.svc.local/healthz   " }

  it 'returns all endpoints' do
    subject.get

    first_message = subject.outputter.messages.first.split("\n")

    expect(first_message.find {|eps| eps.include?(expected) }).to_not be_nil
  end
end
