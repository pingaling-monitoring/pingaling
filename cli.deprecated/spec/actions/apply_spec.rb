require 'spec_helper'
require 'support/manifest_factory'
require 'actions/apply'

RSpec.describe Actions::Apply do
  subject { described_class.new }

  context 'yaml file with single resource to be created' do
    let(:manifest) { ManifestFactory.create_single_resource }

    it 'creates the resource' do
      subject.apply(manifest: manifest)
      expect(subject.outputter.messages.last).to include('created')
    end
  end

  context 'yaml file with single resource to be updated' do
    let(:manifest) { ManifestFactory.update_single_resource }

    it 'updates the resource' do
      subject.apply(manifest: manifest)
      expect(subject.outputter.messages.last).to include('updated')
    end
  end

  context 'yaml file with three resources' do
    let(:manifest) { ManifestFactory.stream_of_manifests }
    let(:gw_spy) { double(Gateway) }

    before do
      allow(gw_spy).to receive(:post_manifest).and_return(OpenStruct.new(body: {'name' => 'foobar'}.to_json, status: OpenStruct.new(code: 200)))
      allow_any_instance_of(described_class).to receive(:gw).and_return(gw_spy)
    end

    it 'calls the gateway three times' do
      subject.apply(manifest: manifest)

      expect(gw_spy).to have_received(:post_manifest).exactly(3).times
    end
  end
end
