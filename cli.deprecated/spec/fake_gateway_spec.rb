require 'spec_helper'
require 'json'
require 'fake_gateway'

RSpec.describe FakeGateway do
  describe 'an API call without a request body' do
    subject { JSON.parse(described_class.new.get_health_summary.body).fetch("data") }

    let(:health_summary) do
      {
        "data" => [
          {
            "url"     => "https://service.svc.local/healthz",
            "updated" => nil,
            "type"    => "endpoint",
            "status"  => "pending",
            "name"    => "my-service21"
          },
          {
            "url"     => "http://foobar.com.au/diagnostic",
            "updated" => nil,
            "type"    => "endpoint",
            "status"  => "pending",
            "name"    => "my-service22"
          }
        ]
      }
    end

    it 'returns the doc value for getting health summaries' do
      aggregate_failures do
        expect(subject.size).to be >= 1
        expect(subject.first.fetch("url")).to_not be_nil
        expect(subject.first.fetch("type")).to_not be_nil
        expect(subject.first.fetch("status")).to_not be_nil
        expect(subject.first.fetch("name")).to_not be_nil
      end
    end
  end

  describe 'an API call that has a request body' do
    subject { JSON.parse(described_class.new.post_manifest({'spec' => {'name' => 'foobar'}}).body) }

    it 'returns the JSON of the response section' do
      aggregate_failures do
        expect(subject).to have_key('name')
        expect(subject).to have_key('description')
      end
    end
  end
end
