require 'commands/base'
require 'actions/apply'
require 'yaml'

module Commands
  class Apply < Base
    option ['-f', '--file'], 'FILE', 'Path to the yaml file', attribute_name: :manifest, required: true do |arg|
      validate_file(arg)
    end

    def execute
      Actions::Apply.new.apply(manifest: manifest)
    end

    private

    def validate_file(path)
      if File.exist?(path)
        begin
          file = File.read(path)
          content = YAML.load_stream(file)
          content.each { |manifest| manifest.fetch('apiVersion') }
          manifest = file
        rescue
          raise(ArgumentError, "'#{path}' is not a valid file")
        end

        manifest
      else
        raise(ArgumentError, "'#{path}' does not exist")
      end
    end
  end
end
