# frozen_string_literal: true

require 'factory'
require 'actions/base'
require 'tty-table'
require 'json'

module Actions
  class Apply < Base
    def apply(manifest:)
      content = YAML.load_stream(manifest)
      content.each do |single_manifest|
        result = gw.post_manifest(single_manifest)
        parse_result(result)
      end
    end

    private

    def parse_result(result)
      verb = result.status.code == 201 ? 'created' : 'updated'
      response = JSON.parse(result.body)
      outputter.write "#{response.fetch('name')} #{verb}"
    end
  end
end
