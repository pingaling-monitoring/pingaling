defmodule Api.Notifications.NotificationDispatcher do
  import Ecto.Query, warn: false

  require Logger

  alias Api.Repo
  alias Api.Resources.Cronjob
  alias Api.Resources.Endpoint
  alias Api.Notifications.NotificationPolicy
  alias Api.NotificationChannels.Pagerduty
  alias Api.NotificationChannels.Slack

  def dispatch({status, incident, %Cronjob{} = cronjob}) do
    Repo.all(
      from np in NotificationPolicy,
      where: np.cronjob_id == ^cronjob.id,
      preload: [:cronjob, :notification_channel]
    )
    |> dispatch_by_policy(incident, status, cronjob)
  end

  def dispatch({status, incident, %Endpoint{} = endpoint}) do
    Repo.all(
      from np in NotificationPolicy,
      where: np.endpoint_id == ^endpoint.id,
      preload: [:endpoint, :notification_channel]
    )
    |> dispatch_by_policy(incident, status, endpoint)
  end

  defp dispatch_by_policy(notification_policies, incident, status, resource) do
    notification_policies
    |> Enum.map(
         fn np ->
           if np.limit_sending == true do
             Logger.debug("This is where we would check if the time is appropriate to send")
           else
             type = np.notification_channel.type
             dispatch_notification({type, status, %{incident: incident, notification_policy: np, resource: resource}})
           end
         end
       )
  end

  defp dispatch_notification({:pagerduty, status, %{incident: incident, notification_policy: np, resource: resource}}) do
    Pagerduty.send({status, %{incident: incident, resource: resource, policy: np}})
  end

  defp dispatch_notification({:slack, status, %{incident: incident, notification_policy: np, resource: resource}}) do
    Slack.send({status, %{incident: incident, resource: resource, policy: np}})
  end
end
