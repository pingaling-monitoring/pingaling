defmodule Api.CheckHandlers.SuccessHandler do
  @moduledoc false

  use Timex

  require Logger

  alias Api.Resources.Cronjob
  alias Api.Resources.Endpoint
  alias Api.Resources.HealthStatus
  alias Api.Repo

  def handle(%Endpoint{} = endpoint) do
    %HealthStatus{}
    |> HealthStatus.changeset(%{status: :healthy, endpoint_id: endpoint.id})
    |> Repo.insert!
  end

  def handle(%Cronjob{} = cronjob) do
    Logger.info("Received success from cronjob #{cronjob.name}")
    Repo.transaction fn ->
      %HealthStatus{}
      |> HealthStatus.changeset(%{status: :healthy, type: :cron, cronjob_id: cronjob.id})
      |> Repo.insert!

      cronjob
      |> Cronjob.changeset(%{will_alert_at: next_alert_at(cronjob), last_success: Timex.now})
      |> Repo.update!
    end

    unhealthies = Api.Resources.HealthSummariser.unhealthy_cronjobs()
    |> Enum.map(fn cron -> cron.name end)
    Logger.info("Unhealthy cronjobs - #{unhealthies}")
  end

  defp next_alert_at(cronjob) do
    time = Timex.shift(Timex.now, alert_params(cronjob))
    Logger.info("Adjusting next alert time for #{cronjob.name} by #{alert_params(cronjob)|> inspect}. It's currently #{Timex.now}, new alert time is #{time}")
    time
  end

  defp alert_params(%Cronjob{} = cronjob) do
    %{
      hours: cronjob.hours_to_fail_after,
      minutes: cronjob.minutes_to_fail_after,
      days: cronjob.days_to_fail_after
    }
    |> Enum.filter(fn {_, v} -> v != nil end)
  end
end
