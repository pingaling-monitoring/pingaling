defmodule Api.CheckHandlers.FailureHandler do
  @moduledoc false

  require Logger

  alias Api.Resources.Cronjob
  alias Api.Resources.Endpoint
  alias Api.Resources.HealthStatus
  alias Api.Repo

  def handle(%Endpoint{} = endpoint) do
    %HealthStatus{}
    |> HealthStatus.changeset(%{status: :unhealthy, endpoint_id: endpoint.id})
    |> Repo.insert!
  end

  def handle(%Cronjob{} = cronjob) do
    Logger.info("Received failure from cronjob #{cronjob.name}")
    Repo.transaction fn ->
      %HealthStatus{}
      |> HealthStatus.changeset(%{status: :unhealthy, cronjob_id: cronjob.id})
      |> Repo.insert!

      cronjob
      |> Cronjob.changeset(%{last_failure: DateTime.utc_now})
      |> Repo.update
    end
  end
end
