defmodule Api.Repositories.Endpoint do
  import Ecto.Query, warn: false
  alias Api.Repo
  alias Api.Resources.Endpoint
  alias Api.Resources.EndpointHealthStatus

  @doc """
  Creates a endpoint.

  ## Examples

      iex> create_endpoint(%{field: value})
      {:ok, %Endpoint{}}

      iex> create_endpoint(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create(attrs \\ %{}) do
    %Endpoint{}
    |> Endpoint.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:health_statuses, [%{status: :pending}])
    |> Repo.insert()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking endpoint changes.

  ## Examples

      iex> change_endpoint(endpoint)
      %Ecto.Changeset{source: %Endpoint{}}

  """
  def change_endpoint(%Endpoint{} = endpoint) do
    Endpoint.changeset(endpoint, %{})
  end

  @doc """
  Deletes a Endpoint.

  ## Examples

      iex> delete_endpoint(endpoint)
      {:ok, %Endpoint{}}

      iex> delete_endpoint(endpoint)
      {:error, %Ecto.Changeset{}}

  """
  def delete(%Endpoint{} = endpoint) do
    Repo.delete(endpoint)
  end

  @endpoints_query ~S"""
  SELECT
    endpoints.*,
    most_recent_health_status.status,
    most_recent_health_status.updated_at
  FROM endpoints
  JOIN (
    SELECT DISTINCT ON (endpoint_id)
      endpoint_id,
      status,
      updated_at
    FROM health_statuses
    ORDER BY endpoint_id, updated_at DESC
  ) as most_recent_health_status
  ON most_recent_health_status.endpoint_id = endpoints.id
  """
  @doc """
  Returns the list of endpoints with latest health.

  ## Examples

      iex> list()
      [%EndpointHealthStatus{}, ...]

  """
  def list do
    endpoints = Ecto.Adapters.SQL.query!(Repo, @endpoints_query, [])

    Enum.map(endpoints.rows, &Repo.load(EndpointHealthStatus, {endpoints.columns, &1}))
  end

  def get(name) do
    with %Endpoint{} = endpoint <- Repo.get_by(Endpoint, name: name) do
      endpoint
      else
      _ -> {:error, :not_found}
    end
  end

  @doc """
  Gets a single endpoint.

  Raises `Ecto.NoResultsError` if the Endpoint does not exist.

  ## Examples

      iex> get_endpoint!(123)
      %Endpoint{}

      iex> get_endpoint!(456)
      ** (Ecto.NoResultsError)

  """
  def get!(name) do
    Repo.get_by!(Endpoint, name: name)
  end

  @doc """
  Updates a endpoint.

  ## Examples

      iex> update_endpoint(endpoint, %{field: new_value})
      {:ok, %Endpoint{}}

      iex> update_endpoint(endpoint, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update(%Endpoint{} = endpoint, attrs) do
    endpoint
    |> Endpoint.changeset(attrs)
    |> Repo.update()
  end
end
