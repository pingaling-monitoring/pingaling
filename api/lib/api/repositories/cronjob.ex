defmodule Api.Repositories.Cronjob do
  import Ecto.Query, warn: false
  alias Api.Repo
  alias Api.Resources.Cronjob
  alias Api.Resources.CronjobHealthStatus

  @cronjobs_query ~S"""
  SELECT
    cronjobs.*,
    most_recent_health_status.status,
    most_recent_health_status.updated_at
  FROM cronjobs
  JOIN (
    SELECT DISTINCT ON (cronjob_id)
      cronjob_id,
      status,
      updated_at
    FROM health_statuses
    ORDER BY cronjob_id, updated_at DESC
  ) as most_recent_health_status
  ON most_recent_health_status.cronjob_id = cronjobs.id
  """
  @doc """
  Returns the list of cronjobs with latest health.

  ## Examples

      iex> list()
      [%CronjobHealthStatus{}, ...]

  """
  def list do
    cronjobs = Ecto.Adapters.SQL.query!(Repo, @cronjobs_query, [])

    Enum.map(cronjobs.rows, &Repo.load(CronjobHealthStatus, {cronjobs.columns, &1}))
  end

  def get(name) do
    with %Cronjob{} = cronjob <- Repo.get_by(Cronjob, name: name) do
      {:ok, cronjob}
    else
      _ -> {:error, :not_found}
    end
  end

  def get!(name) do
    Repo.get_by!(Cronjob, name: name)
  end

  def create(attrs) do
    %Cronjob{}
    |> Cronjob.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:health_statuses, [%{status: :pending}])
    |> Repo.insert()
  end

  def delete(%Cronjob{} = cronjob) do
    cronjob
    |> Repo.delete()
  end

  def update(%Cronjob{} = cronjob, attrs) do
    cronjob
    |> Cronjob.changeset(attrs)
    |> Repo.update()
  end
end
