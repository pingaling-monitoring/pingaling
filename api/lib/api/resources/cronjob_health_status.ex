defmodule Api.Resources.CronjobHealthStatus do
  use Ecto.Schema

  schema "cronjob_health_status" do
    field :name, :string
    field :description, :string
    field :status, HealthStatusEnum
    field :updated_at, :utc_datetime
  end
end
