defmodule Api.Resources.HealthStatus do
  use Ecto.Schema
  import Ecto.Changeset

  alias Api.Resources.Cronjob
  alias Api.Resources.Endpoint

  schema "health_statuses" do
    field :status, HealthStatusEnum
    belongs_to :endpoint, Endpoint
    belongs_to :cronjob, Cronjob

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(health_status, attrs) do
    health_status
    |> cast(attrs, [:status, :endpoint_id, :cronjob_id])
    |> validate_required([:status])
    |> foreign_key_constraint(:endpoint_id)
    |> foreign_key_constraint(:cronjob_id)
  end
end
