defmodule Api.Resources.Incident do
  use Ecto.Schema
  import Ecto.Changeset
  alias Api.Resources.Endpoint
  alias Api.Resources.Cronjob

  schema "incidents" do
    belongs_to :endpoint, Endpoint
    belongs_to :cronjob, Cronjob
    field :status, IncidentStatusEnum, default: :open

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(incident, attrs) do
    incident
    |> cast(attrs, [:status, :endpoint_id, :cronjob_id])
    |> validate_required([:status])
    |> foreign_key_constraint(:endpoint)
    |> foreign_key_constraint(:cronjob)
  end
end
