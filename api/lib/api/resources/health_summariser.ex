defmodule Api.Resources.HealthSummariser do
  @moduledoc """
  Gets the current health of all resources
  """

  import Ecto.Query, warn: false
  require Logger

  alias Api.Repo
  alias Api.Resources.Cronjob
  alias Api.Resources.Endpoint
  alias Api.Resources.HealthStatus
  alias Api.Resources.CronjobHealthStatus
  alias Api.Resources.EndpointHealthStatus

  @cronjobs_query ~S"""
  SELECT
    name,
    status,
    most_recent_health_status.updated_at
  FROM cronjobs
  JOIN (
    SELECT DISTINCT ON (cronjob_id)
      cronjob_id,
      status,
      updated_at
    FROM health_statuses
    ORDER BY cronjob_id, updated_at DESC
  ) as most_recent_health_status
  ON most_recent_health_status.cronjob_id = cronjobs.id
  """

  @endpoints_query ~S"""
  SELECT
    name,
    status,
    url,
    most_recent_health_status.updated_at
  FROM endpoints
  JOIN (
    SELECT DISTINCT ON (endpoint_id)
      endpoint_id,
      status,
      updated_at
    FROM health_statuses
    ORDER BY endpoint_id, updated_at DESC
  ) as most_recent_health_status
  ON most_recent_health_status.endpoint_id = endpoints.id
  """

  @unhealthy_cronjobs_query ~S"""
  SELECT * FROM cronjobs
  JOIN (
    SELECT DISTINCT ON (cronjob_id)
      cronjob_id,
      status
    FROM health_statuses
    ORDER BY cronjob_id, updated_at DESC
  ) as most_recent_health_status
  ON most_recent_health_status.cronjob_id = cronjobs.id
  WHERE status = 'unhealthy'
  """

  @unhealthy_endpoints_query ~S"""
  SELECT * FROM endpoints
  JOIN (
    SELECT DISTINCT ON (endpoint_id)
      endpoint_id,
      status
    FROM health_statuses
    ORDER BY endpoint_id, updated_at DESC
  ) as most_recent_health_status
  ON most_recent_health_status.endpoint_id = endpoints.id
  WHERE status = 'unhealthy'
  """

  def find() do
    cronjobs = Ecto.Adapters.SQL.query!(Repo, @cronjobs_query, [])
    cronjob_results = Enum.map(cronjobs.rows, &Repo.load(CronjobHealthStatus, {cronjobs.columns, &1}))

    cronjob_results ++ endpoints()
  end

  def endpoints() do
    endpoints = Ecto.Adapters.SQL.query!(Repo, @endpoints_query, [])

    Enum.map(endpoints.rows, &Repo.load(EndpointHealthStatus, {endpoints.columns, &1}))
  end

  def recent_health_statuses(%Endpoint{} = endpoint) do
    recent_health_statuses(endpoint, endpoint.retries)
  end

  def recent_health_statuses(resource, limit \\ 1) do
    Repo.all(
      from health_status in HealthStatus,
      where: field(health_status, ^get_fk(resource)) == ^resource.id,
      order_by: [
        desc: health_status.updated_at
      ],
      limit: ^limit
    )
    |> Enum.reverse
  end

  def recent_health_status(resource) do
    Repo.one(
      from health_status in HealthStatus,
      where: field(health_status, ^get_fk(resource)) == ^resource.id,
      order_by: [
        desc: health_status.updated_at
      ],
      limit: 1
    )
  end

  def resource_healthy?(resource) do
    !resource_unhealthy?(resource)
  end

  def resource_unhealthy?(%Cronjob{} = resource) do
    unhealthy_cronjobs()
    |> Enum.map(fn unhealthy_cronjob -> unhealthy_cronjob.id end)
    |> Enum.member?(resource.id)
  end

  def resource_unhealthy?(%Endpoint{} = resource) do
    unhealthy_endpoints()
    |> Enum.map(fn unhealthy_endpoint -> unhealthy_endpoint.id end)
    |> Enum.member?(resource.id)
  end

  def unhealthy_endpoints() do
    endpoints = Ecto.Adapters.SQL.query!(Repo, @unhealthy_endpoints_query, [])

    Enum.map(endpoints.rows, &Repo.load(Endpoint, {endpoints.columns, &1}))
  end

  def unhealthy_cronjobs() do
    cronjobs = Ecto.Adapters.SQL.query!(Repo, @unhealthy_cronjobs_query, [])

    Enum.map(cronjobs.rows, &Repo.load(Cronjob, {cronjobs.columns, &1}))
  end

  defp get_fk(%Cronjob{} = _), do: :cronjob_id
  defp get_fk(%Endpoint{} = _), do: :endpoint_id
end
