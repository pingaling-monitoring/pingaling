defmodule Api.Resources.Cronjob do
  use Ecto.Schema

  import Ecto.Changeset

  alias Api.Resources.HealthStatus
  alias Api.Resources.Incident

  schema "cronjobs" do
    field :description, :string
    field :name, :string

    field :minutes_to_fail_after, :integer
    field :hours_to_fail_after, :integer
    field :days_to_fail_after, :integer

    field :last_success, :utc_datetime
    field :last_failure, :utc_datetime
    field :will_alert_at, :utc_datetime

    has_many :health_statuses, HealthStatus, on_delete: :delete_all
    has_many :incidents, Incident, on_delete: :delete_all

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(cronjob, attrs) do
    cronjob
    |> cast(
         attrs,
         [
           :days_to_fail_after,
           :description,
           :hours_to_fail_after,
           :last_failure,
           :last_success,
           :minutes_to_fail_after,
           :name,
           :will_alert_at
         ]
       )
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
