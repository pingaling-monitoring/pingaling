defmodule Api.Resources.EndpointHealthStatus do
  use Ecto.Schema

  schema "endpoint_health_status" do
    field :name, :string
    field :status, HealthStatusEnum
    field :url, :string
    field :description, :string
    field :updated_at, :utc_datetime
    field :next_check, :utc_datetime
  end
end
