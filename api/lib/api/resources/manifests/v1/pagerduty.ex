defmodule Api.Resources.Manifests.V1.Pagerduty do
  import Ecto.Query, warn: false
  import Ecto.Changeset
  alias Api.Notifications.ChannelConfiguration
  alias Api.Repo

  @doc false
  def changeset(pagerduty, attrs) do
    pagerduty
    |> cast(
         attrs,
         [
           :name,
           :type,
           :data
         ]
       )
    |> validate_required([:name, :type, :data])
    |> unique_constraint(:name)
  end

  def upsert(spec) do
    with {:ok, spec} <- ensure_spec_valid(spec),
         {:ok, spec} <- ensure_name_valid(spec)
      do
      upsert_channel(spec)
    else
      err -> err
    end
  end

  defp attrs_from_spec(spec) do
    %{
      name: Map.get(spec, "name"),
      type: :pagerduty,
      data: %{
        integration_key: Map.get(spec, "integration_key")
      }
    }
  end

  defp ensure_name_valid(%{"name" => name} = spec) do
    if Regex.match?(~r/^[a-z-_]+$/, name) do
      {:ok, spec}
    else
      {:bad_request, %{message: "name must be lowercase with either dashes or underscores"}}
    end
  end

  defp ensure_spec_valid(%{"name" => _, "integration_key" => key} = spec) do
    if valid_integration_key?(key) do
      {:ok, spec}
    else
      {:bad_request, %{message: "Integration key must be 32 lowercase letters and numbers"}}
    end
  end
  defp ensure_spec_valid(%{"integration_key" => _}), do: {:bad_request, %{message: "Missing name"}}
  defp ensure_spec_valid(%{"name" => _}),
       do: {
         :bad_request,
         %{
           message: "Missing integration key. Please create a Service that has a 'V2 Events' API integration in PagerDuty and supply the key"
         }
       }

  defp find_channel(name) do
    chan = Repo.get_by(
      ChannelConfiguration,
      name: name
    )

    if is_nil(chan) do
      nil
    else
      {:ok, chan}
    end
  end

  defp operation(name) do
    with {:ok, pagerduty} <- find_channel(name) do
      {:update, pagerduty}
    else
      nil -> {:create, name}
    end
  end

  defp perform({:create, _}, attrs) do
    pagerduty = %ChannelConfiguration{}
                |> changeset(attrs)
                |> Repo.insert!
    {:created, pagerduty}
  end

  defp perform({:update, pagerduty}, attrs) do
    updated = pagerduty
              |> changeset(attrs)
              |> Repo.update!
    {:ok, updated}
  end

  defp result({status, resource}) do
    {
      status,
      %{
        name: resource.name,
        integration_key: resource.data.integration_key
      }
    }
  end

  defp upsert_channel(%{"name" => name} = spec) do
    attrs = attrs_from_spec(spec)
            |> Map.merge(%{"name" => name})

    attrs_keyed_with_strings = for {key, val} <- attrs, into: %{}, do: {Kernel.to_string(key), val}

    operation(name)
    |> perform(attrs_keyed_with_strings)
    |> result
  end

  defp valid_integration_key?(value) do
    Regex.match?(~r/\A[\da-z]{32}\z/, value)
  end
end

