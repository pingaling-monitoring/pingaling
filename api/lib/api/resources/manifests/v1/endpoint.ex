defmodule Api.Resources.Manifests.V1.Endpoint do
  alias Api.Resources.Endpoint
  alias Api.Repositories.Endpoint, as: EndpointRepo

  def upsert(params) do
    {status, spec} = ensure_spec_valid(params)
    name = spec["name"]

    if status == :bad_request do
      {status, spec}
    else
      operation(name)
      |> perform(spec)
      |> result
    end
  end

  defp ensure_spec_valid(%{"name" => name, "url" => _} = spec) do
    if Regex.match?(~r/^[a-z-_]+$/, name) do
      {:ok, spec}
    else
      {:bad_request, %{message: "name must be lowercase with either dashes or underscores"}}
    end
  end

  defp ensure_spec_valid(%{"name" => _}), do: {:bad_request, %{message: "spec is missing url"}}
  defp ensure_spec_valid(%{"url" => _}), do: {:bad_request, %{message: "spec is missing name"}}
  defp ensure_spec_valid(_), do: {:bad_request, %{message: "spec is not a map"}}

  defp operation(name) do
    with %Endpoint{} = endpoint <- EndpointRepo.get(name) do
      {:update, endpoint}
    else
      {:error, :not_found} -> {:create, name}
      err -> err
    end
  end

  defp perform({:create, _}, attrs) do
    {_, endpoint} = EndpointRepo.create(attrs)
    {:created, endpoint}
  end

  defp perform({:update, endpoint}, attrs) do
    {_, endpoint} = EndpointRepo.update(endpoint, attrs)
    {:ok, endpoint}
  end

  defp result({status, resource}) do
    {
      status,
      %{
        description: resource.description,
        name: resource.name,
        url: resource.url
      }
    }
  end
end
