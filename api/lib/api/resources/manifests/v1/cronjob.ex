defmodule Api.Resources.Manifests.V1.Cronjob do
  alias Api.Repositories.Cronjob, as: CronjobRepo

  def upsert(spec) do
    with {:ok, spec} <- ensure_spec_valid(spec),
         {:ok, spec} <- ensure_name_valid(spec)
      do
      upsert_cronjob(spec)
    else
      err -> err
    end
  end

  defp ensure_spec_valid(%{"name" => _} = spec), do: {:ok, spec}
  defp ensure_spec_valid(_), do: {:bad_request, %{message: "spec is not a map"}}

  defp ensure_name_valid(%{"name" => name} = spec) do
    if Regex.match?(~r/^[a-z-_]+$/, name) do
      {:ok, spec}
    else
      {:bad_request, %{message: "name must be lowercase with either dashes or underscores"}}
    end
  end

  defp failure_params(
         %{
           "alert_without_success" => %{
             "minutes" => minutes
           }
         }
       ), do: %{minutes_to_fail_after: minutes, hours_to_fail_after: 0}
  defp failure_params(
         %{
           "alert_without_success" => %{
             "minutes" => minutes,
             "hours" => hours
           }
         }
       ), do: %{minutes_to_fail_after: minutes, hours_to_fail_after: hours}
  defp failure_params(
         %{
           "alert_without_success" => %{
             "minutes" => minutes,
             "hours" => hours,
             "days" => days
           }
         }
       ), do: %{minutes_to_fail_after: minutes, hours_to_fail_after: hours, days_to_fail_after: days}
  defp failure_params(
         %{
           "alert_without_success" => %{
             "days" => days
           }
         }
       ), do: %{days_to_fail_after: days, hours_to_fail_after: 0}
  defp failure_params(
         %{
           "alert_without_success" => %{
             "days" => days,
             "hours" => hours
           }
         }
       ), do: %{days_to_fail_after: days, hours_to_fail_after: hours}
  defp failure_params(
         %{
           "alert_without_success" => %{
             "hours" => hours
           }
         }
       ), do: %{hours_to_fail_after: hours}
  defp failure_params(_), do: %{} # db will set it to 1 hour

  defp other_params(%{"description" => _} = others), do: others
  defp other_params(_), do: %{}

  defp operation(name) do
    with {:ok, cronjob} <- CronjobRepo.get(name) do
      {:update, cronjob}
    else
      {:error, :not_found} -> {:create, name}
    end
  end

  defp perform({:create, _}, attrs) do
    {:ok, cronjob} = CronjobRepo.create(attrs)
    {:created, cronjob}
  end

  defp perform({:update, cronjob}, attrs) do
    CronjobRepo.update(cronjob, attrs)
  end

  defp result({status, resource}) do
    {
      status,
      %{
        description: resource.description,
        name: resource.name
      }
    }
  end

  defp upsert_cronjob(%{"name" => name} = spec) do
    attrs = failure_params(spec)
            |> Map.merge(%{"name" => name})
            |> Map.merge(other_params(spec))

    attrs_keyed_with_strings = for {key, val} <- attrs, into: %{}, do: {Kernel.to_string(key), val}

    operation(name)
    |> perform(attrs_keyed_with_strings)
    |> result
  end
end
