defmodule Api.Checkers.Cronjob do
  use GenServer
  alias Api.CheckHandlers.FailureHandler
  import Ecto.Query, warn: false
  alias Api.Repo
  alias Api.Resources.Cronjob
  require Logger

  @check_every 3_000 # seconds

  @doc """
  Start the checking loop
  """
  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Process.send_after(self(), :work, @check_every)
    {:ok, %{last_run_at: nil}}
  end

  def handle_info(:work, _state) do
    Process.send_after(self(), :work, @check_every)
    Enum.map(failed_cronjobs(), fn cronjob -> FailureHandler.handle(cronjob) end)

    {:noreply, %{last_run_at: :calendar.local_time()}}
  end

  def failed_cronjobs() do
    query = from cronjobs in Cronjob,
                 where: cronjobs.will_alert_at < fragment("timezone('UTC', now())")

    Repo.all(query)
  end
end
