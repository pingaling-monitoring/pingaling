defmodule Api.NotificationChannels.Pagerduty do
  require Logger
  require Poison
  require HTTPoison

  alias Api.Resources.Incident

  @pagerduty_name "Pingaling Monitoring"
  @url "https://events.pagerduty.com/generic/2010-04-15/create_event.json"

  def send({:open, %{incident: incident, resource: resource, policy: policy}}) do
    body = create_event_body(incident, policy.notification_channel.data["integration_key"], resource)
    Logger.info(inspect(body))
    {:ok, response} = HTTPoison.post(
      @url,
      body
      |> Poison.encode!
    )
    Logger.info("Pagerduty Response #{response.status_code} #{response.body}")
  end

  def send({:auto_resolved, %{incident: incident, policy: policy, resource: resource}}) do
    body = resolve_event_body(policy.notification_channel.data["integration_key"], incident_key(incident, resource))
    Logger.info(inspect(body))
    {:ok, response} = HTTPoison.post(
      @url,
      body
      |> Poison.encode!
    )
    Logger.info("Pagerduty Response #{response.status_code} #{response.body}")
  end

  defp create_event_body(%Incident{} = incident, integration_key, resource) do
    %{
      service_key: integration_key,
      incident_key: incident_key(incident, resource),
      event_type: "trigger",
      description: "#{resource.name} failed",
      client: @pagerduty_name,
    }
  end

  defp incident_key(%Incident{} = incident, resource) do
    "#{resource.name}#{incident.id}"
  end

  defp resolve_event_body(integration_key, incident_key) do
    %{
      service_key: integration_key,
      incident_key: incident_key,
      event_type: "resolve",
      description: "Resource became healthy. Resolved by #{@pagerduty_name}"
    }
  end
end
