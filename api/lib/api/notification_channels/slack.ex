defmodule Api.NotificationChannels.Slack do
  require Logger
  require Poison
  require HTTPoison

  @slack_name "Pingaling Monitoring"

  def send({:open, %{incident: incident, resource: resource, policy: policy}}) do
    webhook_url = policy.notification_channel.data["webhook_url"]
    message = Poison.encode!(%{username: @slack_name, text: ":fire: New alert for #{resource.name} (id=#{incident.id}). #{resource.name} is UNHEALTHY"})
    Logger.info(message)
    HTTPoison.post(webhook_url, message)
  end

  def send({:auto_resolved, %{incident: incident, resource: resource, policy: policy}}) do
    webhook_url = policy.notification_channel.data["webhook_url"]
    message = Poison.encode!(%{username: @slack_name, text: ":green: Incident for #{resource.name} (id=#{incident.id}) resolved by system"})
    Logger.info(message)
    HTTPoison.post(webhook_url, message)
  end
end
