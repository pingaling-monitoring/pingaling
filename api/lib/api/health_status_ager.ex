defmodule Api.HealthStatusAger do
  use GenServer
  import Ecto.Query, warn: false
  alias Api.Repo
  require Logger

  @check_every 3_000 # 10 minutes

  @doc """
  Start the aging loop
  """
  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Process.send_after(self(), :work, @check_every)
    {:ok, %{last_run_at: nil}}
  end

  @delete_old_health_statuses """
  DELETE FROM health_statuses
    WHERE id <= (
      SELECT id
      FROM (
        SELECT id
        FROM health_statuses
        ORDER BY id DESC
        LIMIT 1 OFFSET 10 -- retains this many recent health statuses
      ) old_health_statuses
    )
  """
  def handle_info(:work, _state) do
    Process.send_after(self(), :work, @check_every)
    Ecto.Adapters.SQL.query!(Repo, @delete_old_health_statuses, [])

    {:noreply, %{last_run_at: :calendar.local_time()}}
  end
end
