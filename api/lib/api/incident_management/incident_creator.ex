defmodule Api.IncidentManagement.IncidentCreator do
  use GenServer

  @moduledoc false

  @check_every 3_000 # seconds

  import Ecto.Query, warn: false
  alias Api.Repo
  require Logger

  alias Api.Notifications.NotificationDispatcher
  alias Api.Resources.HealthSummariser
  alias Api.Resources.Incident
  alias Api.Resources.Endpoint
  alias Api.Resources.Cronjob

  @doc """
  Start the loop to create incidents for unhealthy systems
  """
  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Process.send_after(self(), :work, @check_every)
    {:ok, %{last_run_at: nil}}
  end

  def handle_info(:work, _state) do
    Process.send_after(self(), :work, @check_every)

    create_incidents()

    {:noreply, %{last_run_at: :calendar.local_time()}}
  end

  def create_incidents() do
    Enum.concat(HealthSummariser.unhealthy_endpoints(), HealthSummariser.unhealthy_cronjobs())
    |> Enum.map(fn unhealthy_resource -> create_incident_if_required(unhealthy_resource) end)

  end

  defp create_incident_if_required(resource) do
    case incident_count(resource) do
      0 -> create_and_alert(resource)
      1 -> Logger.info("Incident already open for #{resource.name}")
    end
  end

  defp create_and_alert(%Endpoint{} = endpoint) do
    Logger.info("Creating incident for #{endpoint.name}")

    {_, incident} = %Incident{}
                    |> Incident.changeset(%{status: :open, endpoint_id: endpoint.id})
                    |> Repo.insert()

    NotificationDispatcher.dispatch({:open, incident, endpoint})
  end

  defp create_and_alert(%Cronjob{} = cronjob) do
    Logger.info("Creating incident for #{cronjob.name}")

    {_, incident} = %Incident{}
                    |> Incident.changeset(%{status: :open, cronjob_id: cronjob.id})
                    |> Repo.insert()

    NotificationDispatcher.dispatch({:open, incident, cronjob})
  end

  defp get_fk(resource) do
    case resource do
      %Endpoint{} -> "endpoint_id"
      %Cronjob{} -> "cronjob_id"
    end
  end

  defp last_incident_query(resource) do
    """
    SELECT count(*)
    FROM incidents
    WHERE #{get_fk(resource)} = #{resource.id}
    AND status = 'open'
    LIMIT 1
    """
  end

  defp incident_count(resource) do
    Ecto.Adapters.SQL.query!(Repo, last_incident_query(resource), []).rows
    |> List.flatten
    |> List.first
  end
end
