defmodule Api.IncidentManagement.IncidentAutoResolver do
  use GenServer

  @moduledoc false

  @check_every 3_000 # seconds

  import Ecto.Query, warn: false
  alias Api.Repo
  require Logger

  alias Api.Notifications.NotificationDispatcher
  alias Api.Resources.Cronjob
  alias Api.Resources.Endpoint
  alias Api.Resources.HealthSummariser
  alias Api.Resources.Incident

  @doc """
  Start the loop to resolve incidents when endpoints are healthy
  """
  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Process.send_after(self(), :work, @check_every)
    {:ok, %{last_run_at: nil}}
  end

  def handle_info(:work, _state) do
    Process.send_after(self(), :work, @check_every)

    resolve_incidents()

    {:noreply, %{last_run_at: :calendar.local_time()}}
  end

  def resolve_incidents() do
    Repo.all(
      from i in Incident,
      where: [
        status: "open"
      ],
      preload: [:endpoint, :cronjob]
    )
    |> Enum.map(fn incident -> resolve_incident_if_healthy(incident) end)
  end

  defp incident_resource(incident) do
    if !is_nil(incident.endpoint) do
      incident.endpoint
    else
      incident.cronjob
    end
  end

  defp resolve_incident_if_healthy(incident) do
    resource = incident_resource(incident)

    if HealthSummariser.resource_healthy?(resource) do
      resolve_incident(incident, resource)
    end
  end

  defp resolve_incident(incident, %Cronjob{} = cronjob) do
    Logger.info("Resolving incident for #{cronjob.name}")

    incident
    |> Incident.changeset(%{status: :auto_resolved})
    |> Ecto.Changeset.put_assoc(:cronjob, cronjob)
    |> Repo.update!()

    NotificationDispatcher.dispatch({:auto_resolved, incident, cronjob})
  end

  defp resolve_incident(incident, %Endpoint{} = endpoint) do
    Logger.info("Resolving incident for #{endpoint.name}")

    incident
    |> Incident.changeset(%{status: :auto_resolved})
    |> Ecto.Changeset.put_assoc(:endpoint, endpoint)
    |> Repo.update!()

    NotificationDispatcher.dispatch({:auto_resolved, incident, endpoint})
  end
end
