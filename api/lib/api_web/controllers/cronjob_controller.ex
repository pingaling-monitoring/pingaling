defmodule ApiWeb.CronjobController do
  use ApiWeb, :controller
  require Logger
  alias Api.Repositories.Cronjob, as: CronjobRepo
  alias Api.CheckHandlers.FailureHandler
  alias Api.CheckHandlers.SuccessHandler
  alias Api.Resources.HealthSummariser

  action_fallback ApiWeb.FallbackController

  def index(conn, _) do
    render(conn, "index.json", %{cronjobs: CronjobRepo.list})
  end

  def show(conn, %{"name" => name}) do
    with {:ok, cronjob} <- CronjobRepo.get(name) do
      health_summaries = HealthSummariser.recent_health_statuses(cronjob, 10)
      render(conn, "show.json", %{cronjob: cronjob, health_summary: health_summaries})
    end
  end

  def delete(conn, %{"name" => name}) do
    cronjob = CronjobRepo.get!(name)
    CronjobRepo.delete(cronjob)

    json(conn, %{message: "Deleted cronjob #{cronjob.name}"})
  end

  def success(conn, %{"name" => name}) do
    with {:ok, cronjob} <- CronjobRepo.get(name) do
      SuccessHandler.handle(cronjob)
      json(conn, %{message: "Cronjob #{cronjob.name} marked successful"})
    end
  end

  def failure(conn, %{"name" => name}) do
    with {:ok, cronjob} <- CronjobRepo.get(name) do
      FailureHandler.handle(cronjob)
      json(conn, %{message: "Cronjob #{cronjob.name} marked as failed"})
    end
  end
end
