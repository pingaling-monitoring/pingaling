defmodule ApiWeb.VersionController do
  use ApiWeb, :controller

  import Logger

  def index(conn, _params) do
    json conn, %{version: to_string(Application.spec(:api, :vsn)) }
  end
end
