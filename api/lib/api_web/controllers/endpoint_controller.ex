defmodule ApiWeb.EndpointController do
  use ApiWeb, :controller
  require Logger
  alias Api.Resources.Endpoint
  alias Api.Repositories.Endpoint, as: EndpointRepo
  alias Api.Resources.HealthSummariser

  action_fallback ApiWeb.FallbackController

  def index(conn, _) do
    render(conn, "index.json", %{endpoints: EndpointRepo.list})
  end

  def show(conn, %{"name" => name}) do
    with %Endpoint{} = endpoint <- EndpointRepo.get(name) do
      health_summaries = HealthSummariser.recent_health_statuses(endpoint, 10)
      render(conn, "show.json", %{endpoint: endpoint, health_summary: health_summaries})
    end
  end

  def delete(conn, %{"name" => name}) do
    endpoint = EndpointRepo.get!(name)
    EndpointRepo.delete(endpoint)

    json(conn, %{message: "Deleted endpoint #{endpoint.name}"})
  end
end
