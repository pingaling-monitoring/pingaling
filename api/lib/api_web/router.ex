defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ApiWeb do
    pipe_through :api
    delete "/cronjobs/:name", CronjobController, :delete
    delete "/endpoints/:name", EndpointController, :delete
    delete "/notification_channels/:name", NotificationChannelController, :delete
    delete "/notification_policies/:name", NotificationPolicyController, :delete
    get "/cronjobs/:name/success", CronjobController, :success, as: :cronjob_success
    get "/cronjobs/:name/failure", CronjobController, :failure, as: :cronjob_failure
    get "/cronjobs/:name", CronjobController, :show
    get "/cronjobs", CronjobController, :index
    get "/endpoints/:name", EndpointController, :show
    get "/endpoints", EndpointController, :index
    get "/health/summary", HealthSummaryController, :index
    get "/notification_channels", NotificationChannelController, :index
    get "/notification_policies", NotificationPolicyController, :index
    post "/manifest", ManifestController, :apply
    resources "/incidents", IncidentController, only: [:index]
    get "/version", VersionController, :index
  end
end
