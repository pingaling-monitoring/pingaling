defmodule ApiWeb.EndpointView do
  use ApiWeb, :view
  alias ApiWeb.EndpointView
  alias Api.Resources.EndpointHealthStatus

  def render("index.json", %{endpoints: endpoints}) do
    %{data: render_many(endpoints, EndpointView, "endpoint.json")}
  end

  def render("show.json", params = %{endpoint: _, health_summary: _}) do
    %{data: endpoint_to_map(params)}
  end

  def render("endpoint.json", %{endpoint: endpoint, health_summary: health_summary}) do
    %{
      name: endpoint.name,
      url: endpoint.url,
      status: statuses(health_summary),
      description: endpoint.description,
      next_check: endpoint.next_check
    }
  end

  def render("endpoint.json", %{endpoint: %EndpointHealthStatus{}} = %{endpoint: endpoint}) do
    %{
      name: endpoint.name,
      url: endpoint.url,
      description: endpoint.description,
      status: endpoint.status,
      updated_at: endpoint.updated_at,
      next_check: endpoint.next_check
    }
  end

  defp endpoint_to_map(%{endpoint: endpoint, health_summary: health_summary}) do
    %{
      name: endpoint.name,
      url: endpoint.url,
      description: endpoint.description,
      next_check: endpoint.next_check,
    }
    |> Map.merge(statuses(health_summary))
  end

  defp statuses(%EndpointHealthStatus{} = summary), do: %{status: summary.status, updated_at: summary.updated_at}

  defp statuses(health_summaries) do
    current_health = List.last(health_summaries)
    %{
      health_statuses: Enum.map(
        health_summaries,
        fn health_status ->
          %{
            status: health_status.status,
            updated_at: health_status.updated_at
          }
        end
      ),
      status: current_health.status,
      updated_at: current_health.updated_at
    }
  end
end
