defmodule ApiWeb.IncidentView do
  use ApiWeb, :view
  alias ApiWeb.IncidentView
  alias Api.Resources.Cronjob
  alias Api.Resources.Endpoint

  def render("index.json", %{incidents: incidents}) do
    %{data: render_many(incidents, IncidentView, "incident.json")}
  end

  def render("show.json", %{incident: incident}) do
    %{data: render_one(incident, IncidentView, "incident.json")}
  end

  def render("incident.json", %{incident: incident}) do
    incident_for_resource(incident, incident_resource(incident))
  end

  defp incident_for_resource(incident, %Cronjob{} = cronjob) do
    %{
      id: incident.id,
      name: cronjob.name,
      status: incident.status,
      updated_at: incident.updated_at,
    }
  end

  defp incident_for_resource(incident, %Endpoint{} = endpoint) do
    %{
      id: incident.id,
      name: endpoint.name,
      status: incident.status,
      url: endpoint.url,
      updated_at: incident.updated_at,
      next_attempt: endpoint.next_check
    }
  end

  defp incident_resource(incident) do
    if is_nil(incident.cronjob) do
      incident.endpoint
    else
      incident.cronjob
    end
  end
end
