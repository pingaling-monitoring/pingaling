defmodule ApiWeb.CronjobView do
  use ApiWeb, :view
  alias ApiWeb.CronjobView
  alias Api.Resources.CronjobHealthStatus

  def render("index.json", %{cronjobs: cronjobs}) do
    %{data: render_many(cronjobs, CronjobView, "cronjob.json")}
  end

  def render("show.json", params = %{cronjob: _, health_summary: _}) do
    %{data: cronjob_to_map(params)}
  end

  def render("cronjob.json", %{cronjob: cronjob, health_summary: health_summary}) do
    %{
      name: cronjob.name,
      status: health_summary.status,
      description: cronjob.description,
    }
  end

  def render("cronjob.json", %{cronjob: %CronjobHealthStatus{}} = %{cronjob: cronjob}) do
    %{
      name: cronjob.name,
      description: cronjob.description,
      status: cronjob.status,
      updated_at: cronjob.updated_at
    }
  end

  defp cronjob_to_map(%{cronjob: cronjob, health_summary: health_summary}) do
    %{
      name: cronjob.name,
      description: cronjob.description,
    }
    |> Map.merge(statuses(health_summary))
  end

  defp statuses(%CronjobHealthStatus{} = summary), do: %{status: summary.status}

  defp statuses(statuses) do
    %{
      health_statuses: Enum.map(
        statuses,
        fn health_status ->
          %{
            status: health_status.status,
            updated_at: health_status.updated_at
          }
        end
      )
    }
    |> Map.merge(%{status: List.last(statuses).status})
  end
end
