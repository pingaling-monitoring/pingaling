defmodule ApiWeb.HealthSummaryView do
  use ApiWeb, :view
  alias Api.Resources.CronjobHealthStatus
  alias Api.Resources.EndpointHealthStatus
  alias ApiWeb.HealthSummaryView

  def render("index.json", %{health_summaries: health_summaries}) do
    %{data: render_many(health_summaries, HealthSummaryView, "health_summary.json")}
  end

  def render("show.json", %{health_summary: health_summary}) do
    %{data: render_one(health_summary, HealthSummaryView, "health_summary.json")}
  end

  def render("health_summary.json", %{health_summary: %CronjobHealthStatus{} = health_summary}) do
    %{
      name: health_summary.name,
      status: health_summary.status,
      type: "cronjob",
      updated: health_summary.updated_at,
      url: nil
    }
  end

  def render("health_summary.json", %{health_summary: %EndpointHealthStatus{} = health_summary}) do
    %{
      name: health_summary.name,
      status: health_summary.status,
      type: "endpoint",
      updated: health_summary.updated_at,
      url: health_summary.url
    }
  end
end
