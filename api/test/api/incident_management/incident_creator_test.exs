defmodule Api.IncidentManagement.IncidentCreatorTest do
  use Api.DataCase
  import Api.Factory
  alias Api.IncidentManagement.IncidentCreator
  alias Api.Resources.Incident

  describe "endpoints" do
    test "creates incident for unhealthy service" do
      ep = insert(:endpoint)
      insert_list(4, :health_status, %{endpoint: ep, status: :unhealthy})

      IncidentCreator.create_incidents()

      assert Repo.one(
               from i in Incident,
               where: i.endpoint_id == ^ep.id,
               select: count(i.id)
             ) == 1
    end
  end

  describe "cronjobs" do
    test "creates incident for unhealthy cronjob" do
      cronjob = insert(:cronjob)
      insert(:health_status, %{cronjob_id: cronjob.id, status: :unhealthy})

      IncidentCreator.create_incidents()

      assert Repo.one(
               from i in Incident,
               where: i.cronjob_id == ^cronjob.id,
               select: count(i.id)
             ) == 1
    end
  end
end
