defmodule Api.Resources.HealthSummariserTest do
  use Api.DataCase
  import Api.Factory
  alias Api.Resources.HealthSummariser

  describe "individual endpoint" do
    test "returns only the latest health status" do
      ep = insert(:endpoint)
      insert_pair(:health_status, endpoint: ep)

      assert length(HealthSummariser.find()) == 1
    end

    test "returns multiple statuses to form a summary" do
      insert_pair(:endpoint)

      assert length(HealthSummariser.find()) == 2
    end

    test "returns the recent statuses for a single endpoint" do
      ep = insert(:endpoint)
      insert(:health_status, %{endpoint: ep, status: :healthy})
      insert(:health_status, %{endpoint: ep, status: :unhealthy})

      recent = HealthSummariser.recent_health_statuses(ep, 3) |> Enum.map(fn health -> health.status end)

      assert [:pending, :healthy, :unhealthy] == recent
    end

    test "returns the unique statuses for a single endpoint in order" do
      ep = insert(:endpoint)
      insert_pair(:health_status, %{endpoint: ep, status: :unhealthy})
      insert(:health_status, %{endpoint: ep, status: :healthy})

      recent = HealthSummariser.recent_health_statuses(ep, 2) |> Enum.map(fn health -> health.status end)

      assert [:unhealthy, :healthy] == recent
    end
  end

  describe "most recent status for a resource" do
    test "it returns a HealthStatus" do
      ep = insert(:endpoint)
      insert_pair(:health_status, %{endpoint: ep, status: :unhealthy})
      insert(:health_status, %{endpoint: ep, status: :healthy})

      recent = HealthSummariser.recent_health_status(ep)

      assert recent.status == :healthy
    end
  end

  describe "multiple endpoints" do
    test "it finds all the unhealthy ones" do
      healthy_ep = insert(:endpoint)
      insert(:health_status, %{endpoint: healthy_ep, status: :unhealthy})
      insert(:health_status, %{endpoint: healthy_ep, status: :healthy})
      unhealthy_ep = insert(:endpoint)
      insert_list(4, :health_status, %{endpoint: unhealthy_ep, status: :unhealthy})

      unhealthies = HealthSummariser.unhealthy_endpoints()
      [first] = unhealthies

      assert length(unhealthies) == 1
      assert first.name == unhealthy_ep.name
    end
  end

  describe "cronjobs" do
    test "it returns health status for cronjobs" do
      cronjob = insert(:cronjob)
      insert_pair(:health_status, cronjob: cronjob)

      assert length(HealthSummariser.find()) == 1
    end

    test "it finds all the unhealthy ones" do
      healthy_cronjob = insert(:cronjob)
      insert(:health_status, %{cronjob_id: healthy_cronjob.id, status: :unhealthy})
      insert(:health_status, %{cronjob_id: healthy_cronjob.id, status: :healthy})
      unhealthy_cron = insert(:cronjob)
      insert_list(2, :health_status, %{cronjob_id: unhealthy_cron.id, status: :unhealthy})

      unhealthies = HealthSummariser.unhealthy_cronjobs()
      [first] = unhealthies

      assert length(unhealthies) == 1
      assert first.name == unhealthy_cron.name
    end
  end
end
