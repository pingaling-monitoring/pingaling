defmodule Api.CheckHandlers.SuccessHandlerTest do
  use Api.DataCase
  import Api.Factory
  alias Api.CheckHandlers.SuccessHandler
  alias Api.Resources.HealthSummariser
  alias Api.Repositories.Cronjob, as: CronjobRepo

  describe "success handler" do
    test "it creates a :healthy health status" do
      ep_fact = insert(:endpoint)

      SuccessHandler.handle(ep_fact)

      assert HealthSummariser.recent_health_status(ep_fact).status == :healthy
    end

    test "it updates cronjob will_alert_at" do
      {:ok, new_date, _} = DateTime.from_iso8601("2015-11-11T11:11:11+10")
      cronjob = insert(:cronjob, %{will_alert_at: new_date, hours_to_fail_after: 1})

      SuccessHandler.handle(cronjob)

      updated_cronjob = CronjobRepo.get!(cronjob.name)

      assert updated_cronjob.will_alert_at.year != cronjob.will_alert_at.year
    end
  end
end
