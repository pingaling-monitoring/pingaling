defmodule Api.CheckHandlers.FailureHandlerTest do
  use Api.DataCase
  import Api.Factory
  alias Api.CheckHandlers.FailureHandler
  alias Api.Resources.HealthSummariser
  alias Api.Repositories.Cronjob, as: CronjobRepo

  describe "failure handler" do
    test "it creates an :unhealthy health status" do
      ep_fact = insert(:endpoint)

      FailureHandler.handle(ep_fact)

      assert HealthSummariser.recent_health_status(ep_fact).status == :unhealthy
    end

    test "it updates cronjob last_failed" do
      cronjob = insert(:cronjob)

      FailureHandler.handle(cronjob)

      updated_cronjob = CronjobRepo.get!(cronjob.name)

      assert updated_cronjob.last_failure > cronjob.last_failure
    end

    test "it marks cronjob as unhealthy" do
      cronjob = insert(:cronjob)

      FailureHandler.handle(cronjob)
      assert HealthSummariser.recent_health_status(cronjob).status == :unhealthy
    end
  end
end
