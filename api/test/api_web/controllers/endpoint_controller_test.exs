defmodule ApiWeb.EndpointControllerTest do
  use ApiWeb.ConnCase
  import Api.Factory
  alias Api.Resources.HealthSummariser

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "getting an endpoint" do
    test "it finds them all", %{conn: conn} do
      ep1 = insert(:endpoint)
      ep2 = insert(:endpoint)
      insert(:health_status, endpoint: ep1, status: :healthy)
      insert(:health_status, endpoint: ep2, status: :healthy)

      conn = conn
             |> get(endpoint_path(conn, :index))

      assert json_response(conn, 200)["data"]
             |> Enum.count == 2
      assert json_response(conn, 200)["data"]
             |> List.first
             |> Map.get("status") == "healthy"
    end

    test "by name", %{conn: conn} do
      endpoint = insert(:endpoint)
      health = insert(:health_status, endpoint: endpoint, status: :healthy)

      conn = conn
             |> get(endpoint_path(conn, :show, endpoint.name))
             |> doc

      response = json_response(conn, 200)["data"]
      {:ok, updated_at, _} = DateTime.from_iso8601(response["updated_at"])

      assert json_response(conn, 200)
      assert response["name"] == endpoint.name
      assert response["status"] == "healthy"
      assert DateTime.compare(updated_at, health.updated_at) == :eq
    end

    test "returns not_found if endpoint doesn't exist", %{conn: conn} do
      conn = conn
             |> get(endpoint_path(conn, :show, "nah-mate-no-endpoint"))

      assert json_response(conn, 404)
    end
  end

  describe "showing an endpoint" do
    test "it returns some health history", %{conn: conn} do
      ep = insert(:endpoint)
      assert first = HealthSummariser.recent_health_status(ep).status
      insert_pair(:health_status, endpoint: ep, status: :healthy)
      assert last = HealthSummariser.recent_health_status(ep).status

      conn = get(conn, endpoint_path(conn, :show, ep.name))
             |> doc

      assert json_response(conn, 200)["data"]
             |> Map.get("name") == ep.name
      statuses = json_response(conn, 200)["data"]
                 |> Map.get("health_statuses")

      assert length(statuses) == 3 # inserted statuses + initial pending
    end
  end

  describe "deleting an endpoint" do
    test "it deletes the endpoint", %{conn: conn} do
      endpoint = insert(:endpoint)
      insert(:incident, endpoint: endpoint)

      conn = delete(conn, endpoint_path(conn, :delete, endpoint.name))
             |> doc

      assert json_response(conn, 200) == %{"message" => "Deleted endpoint #{endpoint.name}"}
    end
  end
end
