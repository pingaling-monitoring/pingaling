defmodule ApiWeb.HealthSummaryControllerTest do
  use ApiWeb.ConnCase do
    import Api.Factory
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "returning health summaries" do
    test "for all endpoints", %{conn: conn} do
      endpoints = insert_pair(:endpoint)
      [head | _] = endpoints
      insert(:health_status, %{endpoint: head})

      result = conn
               |> get(health_summary_path(conn, :index))
               |> doc
      data = json_response(result, 200)["data"]

      assert data
             |> Enum.count >= 2

      assert data
             |> List.first
             |> Map.get("type") == "endpoint"
    end

    test "for cronjobs", %{conn: conn} do
      cronjobs = insert_pair(:cronjob)
      [head | _] = cronjobs
      insert(:health_status, %{cronjob: head})

      result = conn
               |> get(health_summary_path(conn, :index))

      data = json_response(result, 200)["data"]

      assert data
             |> Enum.count >= 2

      assert data
             |> List.first
             |> Map.get("type") == "cronjob"
    end
  end
end
