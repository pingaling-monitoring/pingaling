defmodule ApiWeb.Manifests.ManifestControllerCronjobsTest do
  use ApiWeb.ConnCase
  @moduledoc false

  alias Api.Resources.HealthSummariser
  alias Api.Repositories.Cronjob, as: CronjobRepo

  @name "periodic-yak-shaver"
  @dodgy_name "the great big yak SHAVE"
  @description "periodic shaving of yaks"
  @minutes 3
  @base %{"apiVersion" => 1, "kind" => "checks/cronjob"}
  @create_attrs Map.merge(
                  @base,
                  %{
                    "spec" => %{
                      "name" => @name,
                      "alert_without_success" => %{
                        "minutes" => @minutes
                      }
                    }
                  }
                )
  @update_attrs Map.merge(
                  @base,
                  %{
                    "spec" => %{
                      "name" => @name,
                      "description" => @description
                    }
                  }
                )
  @dodgy_attrs Map.merge(
                  @base,
                  %{
                    "spec" => %{
                      "name" => @dodgy_name
                    }
                  }
                )

  describe "applying a manifest" do
    test "creates a cronjob" do
      conn = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @create_attrs)
             |> doc
      assert json_response(conn, :created)["name"] == @name
    end

    test "new cronjobs have a pending status" do
      post(build_conn(), manifest_path(build_conn(), :apply), manifest: @create_attrs)
      {:ok, cronjob} = CronjobRepo.get(@name)
      recent = HealthSummariser.recent_health_status(cronjob).status

      assert recent == :pending
    end

    # There's a default value set for hours or whatever
    # Test that if time params are supplied, that it does not include the default
    test "sets the next alert time preciously" do
      post(build_conn(), manifest_path(build_conn(), :apply), manifest: @create_attrs)
      {:ok, cronjob} = CronjobRepo.get(@name)

      assert cronjob.minutes_to_fail_after == @minutes
      assert cronjob.hours_to_fail_after == 0
      assert cronjob.days_to_fail_after == 0
    end

    test "updates a cronjob" do
      create = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @create_attrs)
      assert json_response(create, :created)["description"] == nil

      update = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @update_attrs)
               |> doc

      assert json_response(update, :ok)["description"] == @update_attrs
                                                          |> Map.get("spec")
                                                          |> Map.get("description")
    end
  end

  describe "cronjob with a dodgy name" do
    test "returns bad request" do
      conn = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @dodgy_attrs)
      assert json_response(conn, :bad_request)
    end
  end
end
