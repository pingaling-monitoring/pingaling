defmodule ApiWeb.Manifests.ManifestControllerEndpointsTest do
  use ApiWeb.ConnCase
  @moduledoc false

  @name "foobar-svc"
  @dodgy_name "the zepplin DINGBAT"
  @description "an excellent service"
  @base %{"apiVersion" => 1, "kind" => "checks/endpoint"}
  @create_attrs Map.merge(
                  @base,
                  %{
                    "spec" => %{
                      "url" => "https://google.com",
                      "name" => @name
                    }
                  }
                )
  @update_attrs Map.merge(
                  @base,
                  %{
                    "spec" => %{
                      "url" => "https://google.com",
                      "name" => @name,
                      "description" => @description
                    }
                  }
                )
  @dodgy_attrs Map.merge(
                  @base,
                  %{
                    "spec" => %{
                      "url" => "https://google.com",
                      "name" => @dodgy_name
                    }
                  }
                )
  @lacking_url Map.merge(
                 @base,
                 %{
                   "spec" => %{
                     "name" => @name
                   }
                 }
               )

  describe "applying a manifest" do
    test "creates an endpoint" do
      conn = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @create_attrs)
             |> doc
      assert json_response(conn, :created)["name"] == @name
    end

    test "updates an endpoint" do
      create = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @create_attrs)
      assert json_response(create, :created)["description"] == nil

      update = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @update_attrs)
               |> doc
      assert json_response(update, :ok)["description"] == @update_attrs
                                                          |> Map.get("spec")
                                                          |> Map.get("description")
    end
  end

  describe "endpoint with a dodgy name" do
    test "returns bad request" do
      conn = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @dodgy_attrs)
      assert json_response(conn, :bad_request)
    end
  end

  describe "endpoint missing URL" do
    test "returns bad request" do
      conn = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @lacking_url)
      assert json_response(conn, :bad_request)
    end
  end
end
