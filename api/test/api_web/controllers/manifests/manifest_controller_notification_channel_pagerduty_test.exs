defmodule ApiWeb.Manifests.ManifestControllerNotificationChannelPagerdutyTest do
  use ApiWeb.ConnCase
  @moduledoc false

  @name "foobar"
  @base %{"apiVersion" => 1, "kind" => "notifications/pagerduty"}
  @integration_key "3a2f017d50631f3827cb15638405547d"
  @new_integration_key "7b2f017d50631f3827cb15638405531c"

  @create_attrs Map.merge(
                  @base,
                  %{
                    "spec" => %{
                      "integration_key" => @integration_key,
                      "name" => @name
                    }
                  }
                )
  @update_attrs Map.merge(
                  @base,
                  %{
                    "spec" => %{
                      "name" => @name,
                      "integration_key" => @new_integration_key
                    }
                  }
                )

  describe "applying a manifest" do
    test "creates a pagerduty channel" do
      conn = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @create_attrs)
             |> doc

      assert json_response(conn, :created)["name"] == @name
      assert json_response(conn, :created)["integration_key"] == @integration_key
    end

    test "updates a pagerduty channel" do
      post(build_conn(), manifest_path(build_conn(), :apply), manifest: @create_attrs)
      conn = post(build_conn(), manifest_path(build_conn(), :apply), manifest: @update_attrs)
             |> doc

      assert json_response(conn, :ok)["name"] == @name
      assert json_response(conn, :ok)["integration_key"] == @new_integration_key
    end
  end
end
