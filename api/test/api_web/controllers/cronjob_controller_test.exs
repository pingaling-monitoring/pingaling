defmodule ApiWeb.CronjobControllerTest do
  use ApiWeb.ConnCase
  import Api.Factory

  alias Api.Resources.HealthSummariser
  alias Api.Repositories.Cronjob

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "getting cronjobs" do
    test "all are retrieved", %{conn: conn} do
      cron1 = insert(:cronjob)
      health = insert(:health_status, cronjob: cron1, status: :healthy)
      insert(:cronjob)

      conn = get(conn, cronjob_path(conn, :index))
             |> doc

      assert json_response(conn, 200)["data"]
             |> Enum.count == 2
      assert json_response(conn, 200)["data"]
             |> List.first
             |> Map.get("status") == to_string(health.status)
    end

    test "can be found by name", %{conn: conn} do
      cronjob = insert(:cronjob)
      insert(:health_status, cronjob: cronjob, status: :healthy)

      conn = get(conn, cronjob_path(conn, :show, cronjob.name))
             |> doc

      assert json_response(conn, 200)["data"]
             |> Map.get("name") == cronjob.name
      assert json_response(conn, 200)["data"]
             |> Map.get("status") == "healthy"
    end

    test "returns 404 when not found", %{conn: conn} do
      conn = get(conn, cronjob_path(conn, :show, "foobar-not-cron"))

      assert json_response(conn, 404)
    end
  end

  describe "showing a cronjob" do
    test "it returns some health history", %{conn: conn} do
      cronjob = insert(:cronjob)
      assert first = HealthSummariser.recent_health_status(cronjob).status
      insert_pair(:health_status, cronjob: cronjob, status: :healthy)
      assert last = HealthSummariser.recent_health_status(cronjob).status

      conn = get(conn, cronjob_path(conn, :show, cronjob.name))
             |> doc

      assert json_response(conn, 200)["data"]
             |> Map.get("name") == cronjob.name
      statuses = json_response(conn, 200)["data"]
                 |> Map.get("health_statuses")

      assert length(statuses) == 3 # inserted statuses + initial pending
      assert first == :pending
      assert last == :healthy
    end
  end

  describe "deleting a cronjob" do
    test "it deletes the cronjob", %{conn: conn} do
      cronjob = insert(:cronjob)
      insert(:incident, cronjob: cronjob)

      conn = delete(conn, cronjob_path(conn, :delete, cronjob.name))
             |> doc

      assert json_response(conn, 200) == %{"message" => "Deleted cronjob #{cronjob.name}"}
    end
  end

  describe "marking a cronjob successful" do
    test "it adjusts will_alert_at", %{conn: conn} do
      cronjob = insert(:cronjob)

      conn = get(conn, cronjob_success_path(conn, :success, cronjob.name))
             |> doc
      updated_cronjob = Cronjob.get!(cronjob.name)

      assert json_response(conn, 200) == %{"message" => "Cronjob #{cronjob.name} marked successful"}
      assert updated_cronjob.last_success != cronjob.last_success
    end

    test "it returns not_found if the cronjob doesn't exist", %{conn: conn} do
      conn = get(conn, cronjob_success_path(conn, :success, "umm-nah-mate"))

      assert json_response(conn, 404)
    end
  end

  describe "marking a cronjob as failed" do
    test "status becomes unhealthy", %{conn: conn} do
      cronjob = insert(:cronjob)

      conn = get(conn, cronjob_failure_path(conn, :failure, cronjob.name))
             |> doc
      updated_cronjob = Cronjob.get!(cronjob.name)

      assert json_response(conn, 200) == %{"message" => "Cronjob #{cronjob.name} marked as failed"}
      assert updated_cronjob.last_failure != cronjob.last_failure
      assert HealthSummariser.recent_health_status(cronjob).status == :unhealthy
    end

    test "it returns not_found if the cronjob doesn't exist", %{conn: conn} do
      conn = get(conn, cronjob_success_path(conn, :success, "umm-nah-mate"))

      assert json_response(conn, 404)
    end
  end
end
