# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
alias Api.Repo
alias Api.Repositories.Endpoint, as: EndpointRepo
alias Api.Repositories.Cronjob, as: CronjobRepo
alias Api.Notifications.NotificationChannels

Repo.transaction fn ->
  Repo.insert!(
    %Api.Notifications.ChannelConfiguration {
      name: "pagemyduty",
      type: "pagerduty",
      data: %{
        "integration_key" => "8cb15638405547d8a2f017d50631f382"
      },
    }
  )
  Repo.insert!(
    %Api.Notifications.ChannelConfiguration {
      name: "slackoff",
      type: "slack",
      data: %{
        "webhook_url" => "https://hooks.slack.com/services/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"
      },
    }
  )
  Repo.insert!(
    %Api.Notifications.ChannelConfiguration {
      name: "slacktastic",
      type: "slack",
      data: %{
        "webhook_url" => "https://hooks.slack.com/services/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"
      },
    }
  )
  Repo.insert!(
    %Api.Notifications.ChannelConfiguration {
      name: "slack-bar",
      type: "slack",
      data: %{
        "webhook_url" => "https://hooks.slack.com/services/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"
      },
    }
  )
end

Repo.transaction fn ->
  Repo.insert!(
    %Api.Resources.Endpoint{
      name: "dingbat-poker",
      url: "http://my-service/healthz"
    }
  )
  Repo.insert!(
    %Api.Resources.HealthStatus{
      status: :pending,
      endpoint: EndpointRepo.get!("dingbat-poker")
    }
  )
  Repo.insert!(
    %Api.Resources.HealthStatus{
      status: :unhealthy,
      endpoint: EndpointRepo.get!("dingbat-poker")
    }
  )
  Repo.insert!(
    %Api.Resources.HealthStatus{
      status: :unhealthy,
      endpoint: EndpointRepo.get!("dingbat-poker")
    }
  )
  Repo.insert!(
    %Api.Resources.HealthStatus{
      status: :healthy,
      endpoint: EndpointRepo.get!("dingbat-poker")
    }
  )
  Repo.insert!(
    %Api.Resources.Incident{
      status: :auto_resolved,
      endpoint: EndpointRepo.get!("dingbat-poker")
    }
  )
end

Repo.transaction fn ->
  Repo.insert!(
    %Api.Resources.Endpoint{
      name: "widget-aligner",
      url: "https://google.com.au"
    }
  )
  Repo.insert!(
    %Api.Resources.HealthStatus{
      status: :pending,
      endpoint: EndpointRepo.get!("widget-aligner")
    }
  )
  Repo.insert!(
    %Api.Resources.HealthStatus{
      status: :healthy,
      endpoint: EndpointRepo.get!("widget-aligner")
    }
  )
  Repo.insert!(
    %Api.Resources.Incident{
      status: :auto_resolved,
      endpoint: EndpointRepo.get!("widget-aligner")
    }
  )
  Repo.insert!(
    %Api.Notifications.NotificationPolicy {
      name: "alert-widget",
      endpoint_id: EndpointRepo.get!("widget-aligner").id,
      notification_channel: NotificationChannels.get_notification_channel!("slack-bar")
    }
  )
end

Repo.transaction fn ->
  Repo.insert!(
    %Api.Resources.Endpoint{
      name: "foobar-throbbler",
      url: "http://google.com.au"
    }
  )
  Repo.insert!(
    %Api.Resources.HealthStatus{
      status: :pending,
      endpoint: EndpointRepo.get!("foobar-throbbler")
    }
  )
end

Repo.transaction fn ->
  Repo.insert!(
    %Api.Resources.Cronjob{
      name: "scheduled-twibbler"
    }
  )
  Repo.insert!(
    %Api.Resources.HealthStatus{
      status: :unhealthy,
      cronjob: CronjobRepo.get!("scheduled-twibbler")
    }
  )
  Repo.insert!(
    %Api.Notifications.NotificationPolicy {
      name: "scheduled-twibbler",
      cronjob_id: CronjobRepo.get!("scheduled-twibbler").id,
      notification_channel: NotificationChannels.get_notification_channel!("pagemyduty"),
    }
  )
end


Repo.transaction fn ->
  Repo.insert!(
    %Api.Resources.Cronjob{
      name: "scheduled-twinkler"
    }
  )
  Repo.insert!(
    %Api.Resources.HealthStatus{
      status: :pending,
      cronjob: CronjobRepo.get!("scheduled-twinkler")
    }
  )
end

Repo.transaction fn ->
  Repo.insert!(
    %Api.Resources.Endpoint{
      name: "dingbat-bar",
      url: "http://bar-service/healthz"
    }
  )
end

Repo.transaction fn ->
  Repo.insert!(
    %Api.Notifications.NotificationPolicy {
      name: "alert-bar",
      endpoint_id: EndpointRepo.get!("dingbat-bar").id,
      notification_channel: NotificationChannels.get_notification_channel!("slack-bar"),
    }
  )
end
