defmodule Api.Repo.Migrations.DropTypeFromHealthStatuses do
  use Ecto.Migration

  def change do
    alter table(:health_statuses) do
      remove(:type)
    end
  end
end
