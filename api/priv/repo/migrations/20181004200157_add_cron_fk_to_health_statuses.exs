defmodule Api.Repo.Migrations.AddCronFkToHealthStatuses do
  use Ecto.Migration

  def up do
    alter table(:health_statuses) do
      add :cronjob_id, references(:cronjobs, on_delete: :delete_all)
    end
  end

  def down do
    alter table(:health_statuses) do
      remove :cronjob_id
    end
  end
end
