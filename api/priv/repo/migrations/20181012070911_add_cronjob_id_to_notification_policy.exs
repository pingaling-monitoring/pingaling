defmodule Api.Repo.Migrations.AddCronjobIdToNotificationPolicy do
  use Ecto.Migration

  def change do
    alter table(:notification_policies) do
      add :cronjob_id, references(:cronjobs, on_delete: :delete_all)
    end
  end
end
