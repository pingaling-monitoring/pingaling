defmodule Api.Repo.Migrations.CheckAtLeastOneFkOnHealthStatuses do
  use Ecto.Migration

  def up do
    execute """
      ALTER TABLE health_statuses
      ADD CONSTRAINT exactly_one_resource CHECK (array_length(array_remove(ARRAY[endpoint_id, cronjob_id], NULL), 1) = 1);
    """
  end

  def down do
    execute """
      ALTER TABLE health_statuses
      DROP CONSTRAINT exactly_one_resource
    """
  end
end
