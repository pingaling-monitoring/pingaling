defmodule Api.Repo.Migrations.MakeEndpointIdNullableOnHealthStatuses do
  use Ecto.Migration
  def up do
    execute """
      ALTER TABLE health_statuses
      ALTER COLUMN endpoint_id
      DROP NOT NULL
    """
  end

  def down do
    execute """
      ALTER TABLE health_statuses
      ALTER COLUMN endpoint_id
      SET NOT NULL
    """
  end
end
