defmodule Api.Repo.Migrations.CreateCronjob do
  use Ecto.Migration

  def change do
    create table(:cronjobs) do
      add :name, :string, null: false
      add :description, :text
      add :minutes_to_fail_after, :smallint, default: 0, null: false
      add :hours_to_fail_after, :smallint, default: 1, null: false
      add :days_to_fail_after, :smallint, default: 0, null: false
      add :last_success, :timestamptz
      add :last_failure, :timestamptz
      add :will_alert_at, :timestamptz

      timestamps(type: :timestamptz)
    end

    create unique_index(:cronjobs, [:name])
  end
end
