defmodule Api.Repo.Migrations.MakeEndpointIdNullableOnIncidents do
  use Ecto.Migration

  def up do
    execute """
      ALTER TABLE incidents
      ALTER COLUMN endpoint_id
      DROP NOT NULL
    """
    execute """
      ALTER TABLE incidents
      ADD CONSTRAINT exactly_one_resource CHECK (array_length(array_remove(ARRAY[endpoint_id, cronjob_id], NULL), 1) = 1);
    """
  end

  def down do
    execute """
      ALTER TABLE incidents
      DROP CONSTRAINT exactly_one_resource
    """
    execute """
      ALTER TABLE incidents
      ALTER COLUMN endpoint_id
      SET NOT NULL
    """
  end
end
