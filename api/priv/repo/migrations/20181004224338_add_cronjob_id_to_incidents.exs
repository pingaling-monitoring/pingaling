defmodule Api.Repo.Migrations.AddCronjobIdToIncidents do
  use Ecto.Migration

  def up do
    alter table(:incidents) do
      add :cronjob_id, references(:cronjobs, on_delete: :delete_all)
    end
  end

  def down do
    alter table(:incidents) do
      remove :cronjob_id
    end
  end
end
