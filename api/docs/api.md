# API Documentation

  * [API /api/cronjobs](#api-api-cronjobs)
    * [index](#api-api-cronjobs-index)
    * [show](#api-api-cronjobs-show)
    * [delete](#api-api-cronjobs-delete)
    * [success](#api-api-cronjobs-success)
    * [failure](#api-api-cronjobs-failure)
  * [API /api/endpoints](#api-api-endpoints)
    * [show](#api-api-endpoints-show)
    * [delete](#api-api-endpoints-delete)
  * [API /api/health](#api-api-health)
    * [index](#api-api-health-index)
  * [API /api/incidents](#api-api-incidents)
    * [index](#api-api-incidents-index)
  * [API /api/manifest](#api-api-manifest)
    * [apply](#api-api-manifest-apply)
  * [API /api/notification_channels](#api-api-notification_channels)
    * [index](#api-api-notification_channels-index)
    * [delete](#api-api-notification_channels-delete)
  * [API /api/notification_policies](#api-api-notification_policies)
    * [index](#api-api-notification_policies-index)
    * [delete](#api-api-notification_policies-delete)

## API /api/cronjobs
### <a id=api-api-cronjobs-index></a>index
#### getting cronjobs all are retrieved
##### Request
* __Method:__ GET
* __Path:__ /api/cronjobs
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "data": [
    {
      "updated_at": "2018-12-17T13:06:53.049359Z",
      "status": "healthy",
      "name": "my-cron18",
      "description": null
    },
    {
      "updated_at": "2018-12-17T13:06:53.050476Z",
      "status": "pending",
      "name": "my-cron19",
      "description": null
    }
  ]
}
```

### <a id=api-api-cronjobs-show></a>show
#### getting cronjobs can be found by name
##### Request
* __Method:__ GET
* __Path:__ /api/cronjobs/my-cron23
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "data": {
    "status": "healthy",
    "name": "my-cron23",
    "health_statuses": [
      {
        "updated_at": "2018-12-17T13:06:53.073650Z",
        "status": "pending"
      },
      {
        "updated_at": "2018-12-17T13:06:53.074988Z",
        "status": "healthy"
      }
    ],
    "description": null
  }
}
```

#### showing a cronjob it returns some health history
##### Request
* __Method:__ GET
* __Path:__ /api/cronjobs/my-cron22
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "data": {
    "status": "healthy",
    "name": "my-cron22",
    "health_statuses": [
      {
        "updated_at": "2018-12-17T13:06:53.066379Z",
        "status": "pending"
      },
      {
        "updated_at": "2018-12-17T13:06:53.068326Z",
        "status": "healthy"
      },
      {
        "updated_at": "2018-12-17T13:06:53.069082Z",
        "status": "healthy"
      }
    ],
    "description": null
  }
}
```

### <a id=api-api-cronjobs-delete></a>delete
#### deleting a cronjob it deletes the cronjob
##### Request
* __Method:__ DELETE
* __Path:__ /api/cronjobs/my-cron20
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Deleted cronjob my-cron20"
}
```

### <a id=api-api-cronjobs-success></a>success
#### marking a cronjob successful it adjusts will_alert_at
##### Request
* __Method:__ GET
* __Path:__ /api/cronjobs/my-cron24/success
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Cronjob my-cron24 marked successful"
}
```

### <a id=api-api-cronjobs-failure></a>failure
#### marking a cronjob as failed status becomes unhealthy
##### Request
* __Method:__ GET
* __Path:__ /api/cronjobs/my-cron21/failure
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Cronjob my-cron21 marked as failed"
}
```

## API /api/endpoints
### <a id=api-api-endpoints-show></a>show
#### getting an endpoint by name
##### Request
* __Method:__ GET
* __Path:__ /api/endpoints/my-service28
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "data": {
    "url": "https://service.svc.local/healthz",
    "updated_at": "2018-12-17T13:06:53.120200Z",
    "status": "healthy",
    "next_check": null,
    "name": "my-service28",
    "health_statuses": [
      {
        "updated_at": "2018-12-17T13:06:53.119425Z",
        "status": "pending"
      },
      {
        "updated_at": "2018-12-17T13:06:53.120200Z",
        "status": "healthy"
      }
    ],
    "description": null
  }
}
```

#### showing an endpoint it returns some health history
##### Request
* __Method:__ GET
* __Path:__ /api/endpoints/my-service27
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "data": {
    "url": "https://dingbats.svc.local/boop",
    "updated_at": "2018-12-17T13:06:53.115584Z",
    "status": "healthy",
    "next_check": null,
    "name": "my-service27",
    "health_statuses": [
      {
        "updated_at": "2018-12-17T13:06:53.113834Z",
        "status": "pending"
      },
      {
        "updated_at": "2018-12-17T13:06:53.115061Z",
        "status": "healthy"
      },
      {
        "updated_at": "2018-12-17T13:06:53.115584Z",
        "status": "healthy"
      }
    ],
    "description": null
  }
}
```

### <a id=api-api-endpoints-delete></a>delete
#### deleting an endpoint it deletes the endpoint
##### Request
* __Method:__ DELETE
* __Path:__ /api/endpoints/my-service29
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Deleted endpoint my-service29"
}
```

## API /api/health
### <a id=api-api-health-index></a>index
#### returning health summaries for all endpoints
##### Request
* __Method:__ GET
* __Path:__ /api/health/summary
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "data": [
    {
      "url": "https://dingbats.svc.local/boop",
      "updated": "2018-12-17T13:06:53.147329Z",
      "type": "endpoint",
      "status": "pending",
      "name": "my-service30"
    },
    {
      "url": "https://service.svc.local/healthz",
      "updated": "2018-12-17T13:06:53.146803Z",
      "type": "endpoint",
      "status": "pending",
      "name": "my-service31"
    }
  ]
}
```

## API /api/incidents
### <a id=api-api-incidents-index></a>index
#### getting all incidents ordered by least to most recent
##### Request
* __Method:__ GET
* __Path:__ /api/incidents
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "data": [
    {
      "url": "http://foobar.com.au/diagnostic",
      "updated_at": "2018-12-17T13:06:53.155948Z",
      "status": "open",
      "next_attempt": null,
      "name": "my-service34",
      "id": 3632
    },
    {
      "url": "http://foobar.com.au/diagnostic",
      "updated_at": "2018-12-17T13:06:53.156777Z",
      "status": "open",
      "next_attempt": null,
      "name": "my-service34",
      "id": 3633
    },
    {
      "updated_at": "2018-12-17T13:06:53.157354Z",
      "status": "open",
      "name": "my-cron35",
      "id": 3634
    }
  ]
}
```

## API /api/manifest
### <a id=api-api-manifest-apply></a>apply
#### applying a manifest creates a cronjob
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "name": "periodic-yak-shaver",
      "alert_without_success": {
        "minutes": 3
      }
    },
    "kind": "checks/cronjob",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 201
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "name": "periodic-yak-shaver",
  "description": null
}
```

#### applying a manifest updates a cronjob
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "name": "periodic-yak-shaver",
      "description": "periodic shaving of yaks"
    },
    "kind": "checks/cronjob",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "name": "periodic-yak-shaver",
  "description": "periodic shaving of yaks"
}
```

#### applying a manifest creates an endpoint
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "url": "https://google.com",
      "name": "foobar-svc"
    },
    "kind": "checks/endpoint",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 201
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "url": "https://google.com",
  "name": "foobar-svc",
  "description": null
}
```

#### applying a manifest updates an endpoint
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "url": "https://google.com",
      "name": "foobar-svc",
      "description": "an excellent service"
    },
    "kind": "checks/endpoint",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "url": "https://google.com",
  "name": "foobar-svc",
  "description": "an excellent service"
}
```

#### applying a manifest creates a pagerduty channel
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "name": "foobar",
      "integration_key": "3a2f017d50631f3827cb15638405547d"
    },
    "kind": "notifications/pagerduty",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 201
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "name": "foobar",
  "integration_key": "3a2f017d50631f3827cb15638405547d"
}
```

#### applying a manifest updates a pagerduty channel
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "name": "foobar",
      "integration_key": "7b2f017d50631f3827cb15638405531c"
    },
    "kind": "notifications/pagerduty",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "name": "foobar",
  "integration_key": "7b2f017d50631f3827cb15638405531c"
}
```

#### applying a manifest creates a notification policy
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "webhook_url": "https://hooks.slack.com/services/Z027TX47K/ABC1C7WUC/yT8EZZquxq4uEHkfE4gzrBoI",
      "name": "foobar"
    },
    "kind": "notifications/slack",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 201
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Created Slack channel foobar"
}
```

#### applying a manifest updates a notification policy
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "webhook_url": "https://hooks.slack.com/services/Z027TX47K/ABC1C7WUC/yT8EZZquxq4uEHkfE4gzrBoI",
      "name": "foobar",
      "description": "non-critical errors"
    },
    "kind": "notifications/slack",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Updated Slack channel foobar"
}
```

#### applying a manifest creates a notification policy
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "name": "foobar",
      "endpoint": "my-service15",
      "channel": "channel16"
    },
    "kind": "notifications/policy",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 201
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Created new policy linking Endpoint 'my-service15' to Slack channel 'foobar'"
}
```

#### applying a manifest updates a notification policy
##### Request
* __Method:__ POST
* __Path:__ /api/manifest
* __Request headers:__
```
content-type: multipart/mixed; boundary=plug_conn_test
```
* __Request body:__
```json
{
  "manifest": {
    "spec": {
      "name": "foobar",
      "endpoint": "my-service13",
      "description": "non-critical errors",
      "channel": "channel14"
    },
    "kind": "notifications/policy",
    "apiVersion": 1
  }
}
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Updated policy linking Endpoint 'my-service13' to Pagerduty channel 'foobar'"
}
```

## API /api/notification_channels
### <a id=api-api-notification_channels-index></a>index
#### listing all channels
##### Request
* __Method:__ GET
* __Path:__ /api/notification_channels
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "data": [
    {
      "updated_at": "2018-12-17T13:06:53.014359Z",
      "type": "slack",
      "name": "channel10"
    },
    {
      "updated_at": "2018-12-17T13:06:53.013554Z",
      "type": "pagerduty",
      "name": "channel9"
    }
  ]
}
```

### <a id=api-api-notification_channels-delete></a>delete
#### deleting channel is deleted
##### Request
* __Method:__ DELETE
* __Path:__ /api/notification_channels/channel11
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Deleted notification channel channel11"
}
```

## API /api/notification_policies
### <a id=api-api-notification_policies-index></a>index
#### listing all policies
##### Request
* __Method:__ GET
* __Path:__ /api/notification_policies
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "data": [
    {
      "updated_at": "2018-12-17T13:06:52.982328Z",
      "type": "slack",
      "name": "notification_policy0",
      "endpoint": "my-service1",
      "channel": "channel2"
    },
    {
      "updated_at": "2018-12-17T13:06:52.984950Z",
      "type": "pagerduty",
      "name": "notification_policy3",
      "endpoint": "my-service4",
      "channel": "channel5"
    }
  ]
}
```

### <a id=api-api-notification_policies-delete></a>delete
#### deleting policy is deleted
##### Request
* __Method:__ DELETE
* __Path:__ /api/notification_policies/notification_policy6
* __Request headers:__
```
accept: application/json
```

##### Response
* __Status__: 200
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
```
* __Response body:__
```json
{
  "message": "Deleted notification policy notification_policy6"
}
```

